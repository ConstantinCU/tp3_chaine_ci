### ETAPE 1/4 : VÉRIFICATION TAG ###
echo "ETAPE 1/4 -> VERIFICATION TAG"

if [ "$1" ]; then
  RUNNER_TAG="$1"
  echo "Le tag séléctionné est : [$RUNNER_TAG] "
else
  RUNNER_TAG="default_tag_tp3"
  echo "Vous n'avez pas séléctionné de tag en arguments, le tag par défaut est [$RUNNER_TAG]"
fi

RUNNER_CONFIG=gitlab/gitlab-runner/config.toml

# Vérifier présence du fichier config.toml
if ! cat "$RUNNER_CONFIG" 2> /dev/null; then
  echo "Le fichier '$RUNNER_CONFIG' n'existe pas. Vous pouvez créer le runner [$RUNNER_TAG] demandé. Continuez les étapes."
elif cat "$RUNNER_CONFIG" | grep -q "$RUNNER_TAG"; then
  echo "Un runner avec le tag '$RUNNER_TAG' existe déjà. Le script s'arrête."
  exit 1
fi

### ETAPE 2/4 : SAISIE DU TOKEN PAR L'UTILISATEUR ###
echo "ETAPE 2/4 -> SAISIE DU TOKEN PAR L'UTILISATEUR"

echo "Veuillez saisir la valeur de la variable REGISTRATION_TOKEN disponible dans les paramètres CI-CD sur le gitlab:"
read REGISTRATION_TOKEN

# Vérifier si l'utilisateur n'a pas saisi de valeur
if [ -z "$REGISTRATION_TOKEN" ]; then
  echo "Vous n'avez pas saisi de valeur pour REGISTRATION_TOKEN. Le script s'arrête."
  exit 1
fi

export REGISTRATION_TOKEN="$REGISTRATION_TOKEN"

### ETAPE 3/4 : CREATION RUNNER DANS CONTAINER GITLAB-RUNNER ###
echo "ETAPE 3/4 -> CREATION RUNNER DANS CONTAINER GITLAB-RUNNER"

RUNNER_CONFIG=gitlab/gitlab-runner/config.toml

if cat "$RUNNER_CONFIG" | grep -q "$RUNNER_TAG"; then
  echo "Le runner '$RUNNER_TAG' existe déjà. Aucune action nécessaire."
else
  echo "Le runner '$RUNNER_TAG' n'existe pas. Enregistrement en cours..."
  docker exec -it gitlab-runner gitlab-runner register \
    --non-interactive \
    --url "http://gitlabhost/" \
    --registration-token "$REGISTRATION_TOKEN" \
    --executor "docker" \
    --docker-image alpine:latest \
    --description "$RUNNER_TAG" \
    --tag-list "$RUNNER_TAG"
fi

### ETAPE 4/4 : MODIFICATION FICHIER CONFIG.TOML ###
echo "ETAPE 4/4 -> MODIFICATION FICHIER config.toml"

export FILE_PATH="gitlab/gitlab-runner/config.toml"

# Réseau utilisé par le container gitlab-ce 
NETWORK_NAME=$(docker inspect gitlab-ce | grep -A 1 Networks | awk 'NR==2 { gsub(/"/, ""); gsub(/:/, ""); gsub(/{/, ""); gsub(/^[ \t]+/, ""); gsub(/[ \t]+$/, ""); print $0 }' | tr -d '\n')

if [ -f "$FILE_PATH" ]; then
  # Rajout network_mode au fichier config.toml si non présent (4 espaces nécéssaires avant le rajout pour une indentation correcte)
  if ! grep -q "network_mode = \"$NETWORK_NAME\"" "$FILE_PATH"; then
    echo "    network_mode = \"$NETWORK_NAME\"" >> "$FILE_PATH"
    echo "Ligne 'network_mode' ajoutée avec succès au fichier $FILE_PATH."
  else
    echo "La ligne 'network_mode' existe déjà dans le fichier $FILE_PATH. Aucune modification n'est nécessaire."
  fi
  # Rechercher la ligne 'volumes' dans le fichier config.toml et modifier si besoin
  if grep -q 'volumes' $FILE_PATH; then
      # Si la ligne existe, MaJ de la valeur
      sed -i 's|volumes = .*|volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]|' $FILE_PATH
      echo "Ligne 'volume' modifiée avec succès au fichier $FILE_PATH."
  else
    echo "Les infos sont déjà présents." 
  fi

else
  echo "Le fichier $FILE_PATH n'a pas été trouvé. Assurez-vous de spécifier le bon chemin."
fi