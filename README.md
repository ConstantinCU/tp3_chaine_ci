
# Chaîne CI pour une application Python
Ce projet réalise la mise en place d'une chaîne d'intégration continue (CI) pour une application en Python.
La CI permet une automatisation du processus de développement, garantissant une qualité constante du code tout au long du cycle de vie du projet.


## Installation

L'installation se réalise à travers le lancement de deux scripts d'installation avec une étape entre les deux qui necessite des manips manuelles pour configurer la plateforme GitLab généré.

-> Execution du script "install_1.sh"

Le script install_1.sh réalise la première partie de l'installation d'un environnement GitLab local avec Docker. Veuillez suivre attentivement les consignes de l'étape 2.
- ÉTAPE 1/2 : CRÉATION DE DOSSIER ET DOCKER COMPOSE (étape automatique)

- ÉTAPE 2/2 : CONSIGNES DE CONFIGURATION DE GITLAB (étape manuelle)
Le script affiche des consignes pour configurer manuellement GitLab en suivant les étapes décrites ci-dessous :

    1. URL GitLab Local : affiche l'URL de l'interface GitLab local à utiliser (http://localhost:80).

    2. Désactivation de l'Inscription : dans l'interface GitLab local, décochez "Sign-up enabled" pour désactiver la création de nouveaux comptes (http://localhost/admin/application_settings/general#js-signup-settings).

    3. Modification du Nom d'Utilisateur : dans l'interface GitLab local, changez le nom d'utilisateur (http://localhost/-/profile/account).

    4. Modification du Mot de Passe : dans l'interface GitLab, changez le mot de passe (http://localhost/-/profile/password/edit).

    5. Reconnexion avec les Nouveaux Identifiants : recommencez la connexion avec les nouveaux identifiants.

    6. Paramètres d'Import du Compte Admin : dans les paramètres d'import du compte admin, cochez la case pour "url repository" (http://localhost/admin/application_settings/general).

    7. Instructions pour la suite : une fois ces étapes terminées, il faut lancer le deuxième script (install_2.sh) avec la commande sudo pour créer le runner. La commande sudo est nécessaire pour exécuter la partie de configuration sur le fichier config.toml.

Le script "install_2.sh" effectue la deuxième partie de l'installation d'un environnement GitLab local avec Docker. Il configure un GitLab Runner en fonction des préférences de l'utilisateur. Il est conçu pour automatiser la configuration du GitLab Runner après la mise en place de l'environnement GitLab local. Il assure une intégration fluide entre le GitLab et le GitLab Runner, facilitant ainsi l'exécution de pipelines CI/CD.

-> Execution du script "install_2.sh" (les étapes decrites sont executées automatiquement) :

- ÉTAPE 1/4 : VÉRIFICATION DU TAG
Le script commence par vérifier si un tag pour le GitLab Runner a été spécifié en argument. Si non, il utilise un tag par défaut. Il vérifie également si le tag existe déjà dans le fichier de configuration (config.toml). Si c'est le cas, le script s'arrête.

- ÉTAPE 2/4 : SAISIE DU TOKEN PAR L'UTILISATEUR
L'utilisateur est invité à saisir la valeur de la variable REGISTRATION_TOKEN, qui est nécessaire pour enregistrer le GitLab Runner auprès de GitLab. Si aucune valeur n'est saisie, le script s'arrête.

- ÉTAPE 3/4 : CRÉATION DU RUNNER DANS LE CONTENEUR GITLAB-RUNNER
Le script vérifie si le runner avec le tag spécifié existe déjà. S'il n'existe pas, le script utilise la commande docker exec pour enregistrer le runner avec les informations fournies, telles que l'URL GitLab, le token d'enregistrement, l'executor (docker), l'image Docker à utiliser, la description du runner, et les tags associés.

- ÉTAPE 4/4 : MODIFICATION DU FICHIER config.toml
Le script modifie le fichier config.toml en ajoutant la configuration network_mode avec la valeur du réseau utilisé par le conteneur GitLab (gitlab-ce). Cette étape permet au GitLab Runner de communiquer correctement avec le GitLab local. Si la ligne network_mode n'est pas présente dans le fichier, elle est ajoutée avec succès.

Note : Le script utilise la commande docker inspect pour extraire le nom du réseau du conteneur GitLab. Cette information est utilisée pour configurer le network_mode dans le fichier config.toml.

## Vérifier le fonctionnement de la chaîne CI

Vous pouvez ensuite vous connecter sur votre GitHub pour vérifier la présence de l'image qui vient d'être créé.


## Description detaillée des repertoires et fichiers

Fichier de pipeline (gitlab-ci.yml):
-  Utilise l'image Docker officielle de Python 3.9 comme base pour l'environnement d'exécution;
- Un tag par defaut est utilisé pour le runner GitLab;
- Définition des étapes du pipeline : 
    - Étape 1 -- lint : utilisation du Pylint pour vérifier les normes de codage, génération des rapports JSON et HTML, et les stocker en tant qu'artefacts.
    - Étape 2 -- test-radon-cc : utilisation de Radon pour mesurer la complexité cyclomatique, génération du rapport JSON, et stockage en tant qu'artefact.
    - Étape 3 -- test-radon-raw : utilisation de Radon pour détecter les duplications de code, génération des rapports JSON et HTML, et les stocker en tant qu'artefacts.
    - Étape 4 -- test-unit : exécution des tests unitaires, génération du rapport JSON, et stockeage en tant qu'artefact.
    - Étape 5 -- build : utilisation de Docker pour construire l'image Docker à partir du Dockerfile.
    - Étape 6 -- deploy : Utilisation de Docker pour pousser l'image construite sur le Docker Hub après une connexion sécurisée.

Fichier Dockerfile

Le fichier Dockerfile est utilisé pour décrire les étapes nécessaires à la création d'une image Docker. Il spécifie l'environnement de travail, les dépendances, et les commandes à exécuter lors du démarrage du conteneur.

- Utilise l'image Docker officielle de Python 3 comme image de base;
- Définit le répertoire de travail à l'intérieur du conteneur comme /app;
- Copie tous les fichiers du répertoire local app dans le répertoire de travail du conteneur (/app);
- Installe les dépendances listées dans le fichier requirements.txt en utilisant pip. L'option --no-cache-dir permet de minimiser la taille de l'image en évitant de conserver le cache de pip;
- Définit la variable d'environnement FLASK_APP avec la valeur "main". Cela spécifie le point d'entrée de l'application Flask;
- Définit la commande par défaut à exécuter lors du démarrage du conteneur. Lance l'application Flask en écoutant sur toutes les interfaces.

Fichier docker-compose:

Décrit la configuration pour le déploiement de deux services Docker, nommées "web" et "gitlab-runner". Le service "web" déploie une instance GitLab, tandis que le service "gitlab-runner" déploie un runner GitLab associé. Cela crée un environnement complet où GitLab peut être utilisé pour gérer des projets, des dépôts, et où GitLab Runner peut être utilisé pour automatiser l'exécution de pipelines CI/CD. L'utilisation conjointe de GitLab et GitLab Runner permet une intégration continue et un déploiement continu (CI/CD) efficaces.

Description du service "web" :
- Image Docker : Utilise l'image Docker de GitLab Community Edition.
- Redémarrage automatique : Configuré pour redémarrer toujours le conteneur en cas d'échec.
- Nom d'hôte et nom du conteneur : Définit le nom d'hôte et le nom du conteneur comme "gitlabhost" et "gitlab-ce", respectivement.
- Configuration GitLab : Utilise la variable d'environnement GITLAB_OMNIBUS_CONFIG pour définir la configuration GitLab, notamment l'URL externe (external_url 'http://gitlabhost').
- Ports exposés : Expose les ports 80 (HTTP) et 8443 (HTTPS) pour l'accès à GitLab.
- Volumes : Montre des volumes pour persister les données de configuration, les journaux et les données de GitLab.
- Réseau : Utilise un réseau nommé "gitlab" pour la communication avec le service GitLab Runner.

Description du service "gitlab-runner" :
- Image Docker : Utilise l'image Docker de GitLab Runner.
- Redémarrage automatique : Configuré pour redémarrer toujours le conteneur en cas d'échec.
- Dépendance : Dépend du service "web" (GitLab) pour garantir que le service GitLab est opérationnel avant de lancer le runner.
- Volumes : Montre des volumes nécessaires pour communiquer avec le socket Docker et stocker la configuration du runner.
- Réseau : Utilise le même réseau "gitlab" que le service GitLab pour faciliter la communication entre les deux.