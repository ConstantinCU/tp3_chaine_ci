
### ETAPE 1/2 : CREATION DOSSIER ET DOCKER COMPOSE ###
echo "ETAPE 1/2 -> CREATION DOSSIER ET DOCKER COMPOSE"
mkdir -p gitlab
export GITLAB_HOME=$(pwd)/gitlab 
echo "GITLAB_HOME = $GITLAB_HOME"

cd gitlab
cp ../docker-compose.yml .

docker-compose up -d
sleep 10
docker exec -it gitlab-ce grep 'Password:' /etc/gitlab/initial_root_password


### ETAPE 2/2 : CONSIGNES CONFIGURATION GITLAB ###
echo "ETAPE 2/2 -> CONSIGNES CONFIGURATION GITLAB (veuillez suivre les consignes suivantes) :"

echo "Attention, un délai de 3 minutes est necessaire pour que le GitLab local soit accessible"
echo "A ce stade les containers ont été crées, il faut donc configurer le Gitlab local. Allez sur l'url ci-dessous et effectuez les actions indiqués ensuite"
echo "http://localhost:80" 

echo "Dans l'interface Gitlab décochez "Sign-up enabled" pour la création de nouveaux comptes : http://localhost/admin/application_settings/general#js-signup-settings"

echo "Changez le nom d'utilisateur : http://localhost/-/profile/account"

echo "Changez le mot de passe : http://localhost/-/profile/password/edit"

echo "Reconnectez-vous avec les nouveaux idientifiants."

echo "Allez dans les paramètres d'import de votre compte admin **Import and exports settings**, cochez la case pour **url respository** (cela vous permettra de pouvoir mettre votre projet dans votre container gitlab local : http://localhost/admin/application_settings/general"

echo "Une fois ces étapes terminés lancez avec sudo le deuxième script **install_2.sh** qui permettra la création du runner (la commande sudo est necessaire pour executer la partie de configuration sur le fichier **config.toml**)"